const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

app.post('/submit', (req, res) => {
  console.log('Form submitted:', req.body);
  const eventData = {
    eventName: req.body.eventName,
    organizer: req.body.organizer,
    date: req.body.date,
    location: req.body.location,
    phoneNumber: req.body.phoneNumber,
    email: req.body.email,
  };

  try {
    const existingEventDetails = JSON.parse(fs.readFileSync('eventDetails.json', 'utf8'));

    existingEventDetails.push(eventData);
    fs.writeFileSync('eventDetails.json', JSON.stringify(existingEventDetails, null, 2));
  } catch (error) {

      fs.writeFileSync('eventDetails.json', JSON.stringify([eventData], null, 2));
  }
 res.send('Event details submitted successfully! Redirecting...');

  setTimeout(() => {
    res.redirect('/');
  }, 1000);
});

app.get('/events', (req, res) => {
  try {
    const eventDetails = fs.readFileSync('eventDetails.json', 'utf8');
    res.send(eventDetails);
  } catch (error) {
    res.status(500).send('Error reading event details');
  }
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
