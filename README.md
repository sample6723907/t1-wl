# Project Name
Event Management sample page

## Continuous Integration and Deployment (CI/CD) Pipeline

This repository is configured with a CI/CD pipeline to automate the build and deployment process. 

### Prerequisites

Before setting up the CI/CD pipeline, ensure to have the following prerequisites installed on your machine:

- [Node.js](https://nodejs.org/)
- [Docker](https://www.docker.com/)
- [Jenkins](https://www.jenkins.io/) (configured and running)

Required Jenkins plugins:
Git plugin
Pipeline plugin
NodeJS plugin (if using Node.js)

### Setup Instructions

**Clone the Repository:**
   ```bash
   git clone https://gitlab.com/sample6723907/t1-wl.git
   cd t1-wl

docker build -t <image-name> .
docker run -p 3000:3000 -d <image-name>

# Gitlab CI/CD Implementation

- Create `.gitlab-ci.yml`
- Create stages as per your requirement
- Use the `node` image for the process
- Inside scripts tag
    - Installl the node modules using command `npm install`
    - Start the server using `npm start`